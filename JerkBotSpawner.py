import socket, string
import time
import ssl as pySsl
import GlobalSettings

class JerkSocketBot:
    CURRENT_NICK_INDEX = 0;
    IRC = None

    def __init__(self, nickName):
        self.IRC = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.IRC = pySsl.wrap_socket(self.IRC)
        self.nickName = nickName

    def ircMessage(self, msg):
        self.send_data("PRIVMSG " + GlobalSettings.CHANNEL + " :" + msg.lstrip() + "\r\n")

    def irc_conn(self):
        self.IRC.connect((GlobalSettings.SERVER, GlobalSettings.PORT))

    def send_data(self, command):
        self.IRC.send(command + '\n')

    def joinChannel(self):
        self.send_data("JOIN %s" % GlobalSettings.CHANNEL)
        JerkBotSpawner.numberOfBotsConnectionsPending -= 1
        #sleep hack number one
        time.sleep(.2)

    def changeNick(self):
        self.nickName = self.nickName + str(self.CURRENT_NICK_INDEX)
        self.CURRENT_NICK_INDEX += 1
        self.send_data("NICK " + self.nickName)

    def login(self, username='user', password=None, realname='Pythonist', hostname='Helena', servername='Server'):
        self.send_data("PASS %s" % (GlobalSettings.PASSWORD))
        self.send_data("USER %s %s %s %s" % (username, hostname, servername, realname))
        self.send_data("NICK " + self.nickName)

    def disconnect(self):
        self.IRC.close()

    def run(self):
        self.irc_conn()
        self.login(self.nickName)
        while (1):
            buffer = self.IRC.recv(1024)
            msg = string.split(buffer)
            print buffer + "\n"
            if len(msg) >= 2 and msg[1] == '433':
                # nickname already in use
                self.changeNick()
            if ":Welcome" in msg:
                self.joinChannel()
                #peace out of the while loop here otherwise it'll block program execution
                return
            if msg[0] == "PING":  # check if server have sent ping command
                self.send_data("PONG %s" % msg[1])  # answer with pong as per RFC 1459
                return


class JerkBotSpawner:
    comicText= None
    numberOfBotsConnectionsPending = 0
    def __init__(self, comicText ):
        self.comicText = comicText

    def sendComicToChat(self):
        data = self.comicText.split('\n')
        comicArray = []
        names = []
        ourBots = {}
        for line in data:
            if len(line) == 0:
                continue
            if not line.isspace() or not len(line) == 0:
                splitLine = line.split(':')
                characterName = splitLine[0].replace(' ', '_').replace('(', '').replace(')', '').replace('.', '')
                #case of empty name for character in strip, usually a caption
                if characterName.isspace() or len(characterName) ==0:
                    characterName = "blankDude"

                chatMessage = splitLine[1]
                if len(splitLine) >= 3:
                    for i in (2, len(splitLine) - 1):
                        print i
                        chatMessage += splitLine[i]
                        if i == len(splitLine) - 1:
                            break

                comicArray.append({'name': characterName, 'messageText' :chatMessage})
                if characterName in names:
                    continue
                names.append(characterName)

        JerkBotSpawner.numberOfBotsConnectionsPending = len(names)

        for name in names:
            bot = JerkSocketBot(name)
            #connecting each bot to IRC
            bot.run()
            ourBots[name] = bot

        while JerkBotSpawner.numberOfBotsConnectionsPending != 0:
            continue

        for line in comicArray:
            botName = line['name']
            ourBots[botName].ircMessage(line['messageText'])
            #this is a hack, ensuring that the messages go out in the correct order
            time.sleep(.2)

        #another sleep hack, making sure we don't dc before all the messages get sent
        time.sleep(.5)
        for key, value in ourBots.iteritems():
            value.disconnect()